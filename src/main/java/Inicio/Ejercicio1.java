package Inicio;

import odt.Persona;
import rx.Observable;
import rx.functions.Func2;

import java.util.ArrayList;
import java.util.List;

public class Ejercicio1 {
    public static void main(String[] args) {
        List<Persona> personas = new ArrayList<>();
        Persona datoPersona = new Persona(3, "Criss Angel");
        personas.add(datoPersona);
        personas.add(new Persona(30, "Cristian"));
        personas.add(new Persona(33, "Alex"));
        personas.add(new Persona(14, "Cristopher"));

        Observable resultMax = Observable.from(personas)
                .map((result) -> {
                    Persona persona = (Persona) result;
                    System.out.println("Nombre: " +persona.getNombre()+", Edad: "+persona.getEdad()+'\n');
                    return persona.getEdad();
                })
                .reduce(
                        new Func2<Integer, Integer, Integer>() {
                            @Override
                            public Integer call(Integer anterior, Integer actual) {
                                if (anterior > actual) {
                                    return anterior;
                                } else {
                                    return actual;
                                }
                            }
                        }
                );
        resultMax.subscribe((alto) -> {
            System.out.println("La edad mas alta de la lista es: " + alto);
        });
    }

}
