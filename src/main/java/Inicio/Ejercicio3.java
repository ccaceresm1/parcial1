package Inicio;

import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.observables.MathObservable;


//@autor Cristian Cáceres
public class Ejercicio3 {
    public static void main(String[] args) {
        Integer[] numbers = {2,5,6,8,10,35,2,10};

        Observable resultMax = Observable.from(numbers);
        MathObservable
                .from(resultMax)
                .averageInteger(resultMax)
                .subscribe((promedio) -> {
                    System.out.println("El Promedio del precio es: " + promedio);
                });
        System.out.printf("Valores mayores a 10 son: ");
        Observable

                .from(numbers)
                .filter(
                        new Func1<Integer, Boolean>() {
                            @Override
                            public Boolean call(Integer actual) {
                                return actual>=10;
                            }
                        }
                )
                .subscribe((valor)->{
                    System.out.print(valor+" ");
                });
        System.out.println("");

        Observable sumaObservable =
                Observable
                        .from(numbers)
                        .reduce(
                                new Func2<Integer, Integer, Integer>() {
                                    @Override
                                    public Integer call(Integer acumulador, Integer actual) {
                                        return acumulador + actual;
                                    }
                                }
                        );

        sumaObservable.subscribe((sumatoria) -> {
            System.out.println("La suma total: " + sumatoria);
        });
    }

}
