package Inicio;

import java.util.ArrayList;
import odt.Producto;
import java.util.List;
import rx.Observable;
import rx.functions.Func2;


//@autor Cristian Cáceres
public class Ejercicio2 {

    public static void main(String[] args) {
        List<Producto> productos = new ArrayList<>();
        Producto datoProducto = new Producto(0," ");
        productos.add(datoProducto);
        productos.add(new Producto(300, "ps4"));
        productos.add(new Producto(300, "gamecube"));
        productos.add(new Producto(200, "external disk"));
        productos.add(new Producto(800, "laptop"));
        productos.add(new Producto(230, "vr"));

        Observable sumaObservable =
                Observable
                        .from(productos.toArray())
                        .map((result) -> {
                            Producto producto = (Producto)  result;
                            return producto.getPrecio();
                        })
                        .reduce(
                                new Func2<Integer, Integer, Integer>() {
                                    @Override
                                    public Integer call(Integer acumulador, Integer actual) {
                                        return acumulador + actual;
                                    }
                                }
                        );
        sumaObservable.subscribe((sumatoria) -> {
            System.out.println("La suma total: " + sumatoria);
        });


        Observable resultMax = Observable.from(productos)
                .map((result) -> {
                    Producto producto = (Producto)  result;
                    return producto.getPrecio();
                })
                .reduce(
                        new Func2<Integer, Integer, Integer>() {
                            @Override
                            public Integer call(Integer anterior, Integer actual) {
                                if(anterior>actual){
                                    return anterior;
                                }else{
                                    return actual;
                                }
                            }
                        }
                );
        resultMax.subscribe((alto) -> {
            System.out.println("El valor mas alto: " + alto);
        });
    }

}
